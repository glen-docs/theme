const getRoot = (varName) => {
    return getComputedStyle(document.documentElement).getPropertyValue(varName).trim();
}

const setRoot = (varName, varValue) => {
    document.querySelector(":root").style.setProperty(varName,varValue);
}

const getSpeed = (s) => {
    let res;
    let nums = Number(s.replace(/[^\d\.]*/g,""));
    let units = s.toLowerCase().replace(/[^a-z]/g,"");
    units == "s" ? res = nums*1000 : res = nums;
    return res
}

const topBarHeight = () => {
    let headbar = document.querySelector("header");
    let main = document.querySelector("main");
    if(headbar && main){
        setRoot("--Header-Bar-Height",`${headbar.offsetHeight}px`);
        main.style.marginTop = "var(--Header-Bar-Height)";
    }
}

const tippys = (el, pos) => {
    document.querySelectorAll(`[${el}]:not([${el}=''])`)?.forEach(x => {
        if(x.getAttribute(el).trim() !== ""){
            if(pos.trim() == "free"){
                tippy(x, {
                    content: x.getAttribute(el),
                    followCursor: true,
                    arrow: false,
                    offset: [0, 18],
                    moveTransition: "transform 0.1s ease-out",
                })
            } else {
                tippy(x, {
                    content: x.getAttribute(el),
                    placement: pos.trim(),
                    followCursor: false,
                    arrow: false,
                    moveTransition: "transform 0s ease-out",
                })
            }
            
        }
    })
}

/*----- DO STUFF -----*/
document.addEventListener("DOMContentLoaded", () => {
    topBarHeight();

    let trig = document.querySelector("header button.topnav");
    let pop = document.querySelector("header nav.dropdown");
    if(trig && pop){
        trig.addEventListener("click", () => {
            if(!trig.matches(".pressed.ani-done")){
                trig.classList.add("pressed");
                pop.style.display = "block";
                setTimeout(() => {
                    pop.classList.add("open");
                },0)

                setTimeout(() => {
                    trig.classList.add("ani-done");
                },getSpeed(getRoot("--Nav-Dropdown-Animation-Speed")))
            }

            else {
                pop.classList.remove("open");
                setTimeout(() => {
                    pop.style.display = "none";

                    setTimeout(() => {
                        trig.classList.remove("ani-done");
                        trig.classList.remove("pressed");
                    },0)
                    
                },getSpeed(getRoot("--Nav-Dropdown-Animation-Speed")))
            }
        })

        pop.querySelectorAll("li")?.forEach(li => {
            li.addEventListener("mouseover", () => {
                li.parentNode.querySelectorAll(":scope > li")?.forEach(e => {
                    if(e !== li){
                        if(!e.matches(".not-hov")){
                            e.classList.add("not-hov")
                        }
                        
                    }
                })
            })

            li.addEventListener("mouseleave", () => {
                if(!li.matches(".not-hov")){
                    li.parentNode.querySelectorAll(":scope > li")?.forEach(e => {
                        if(e !== li){
                            if(e.matches(".not-hov")){
                                e.classList.remove("not-hov")
                            }
                        }
                    });
                }
                
            })
        })
    }

    document.querySelectorAll("i.color-circle[color]:not([color='']), span.color-circle[color]:not([color=''])")?.forEach(c => {
        let color = c.getAttribute("color");
        if(color.trim() !== ""){
            c.style.backgroundColor = color;
        }
    })

    tippys("aria-label","right");
})//end loaded

popify({
	button: ".bulb-section button", // the selector to trigger the popup
	popup_selector: ".popup", // selector of the actual popup
	fade_speed: 400, // in milliseconds only
	exit_options: {
		exit_button: ".close-popup", // popup's close button
		click_outside_to_exit: true,
		escape_key_to_exit: true
	}
})


window.addEventListener("load", () => {
    topBarHeight();
    setTimeout(() => {
        document.querySelectorAll(".rails-1 .rail, .rails-2 .rail")?.forEach(rail => {
            rail.classList.add("ani");
        })
    },169)
})//ended load

window.addEventListener("resize", () => {
    topBarHeight();
})
